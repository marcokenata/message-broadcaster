from django.shortcuts import render
from .forms import Record_Form
from .models import Routers
from django.http import HttpResponseRedirect

# Create your views here.
response = {}


def index(request):
    response['forms'] = Record_Form
    response['routers'] = Routers.objects.all()
    return render(request, 'index1.html', response)


def add_channel(request):
    form = Record_Form(request.POST or None)
    if (request.method == 'POST'):
        response['notifChannelName'] = request.POST['message']
        todo = Routers(routingKey=response['notifChannelName'], count=0)
        todo.save()
        return HttpResponseRedirect('/home/')
    else:
        return HttpResponseRedirect('/home/')
