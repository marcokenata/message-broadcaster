from django.conf.urls import url
from .views import *
#url for app

app_name = 'channelAdder'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add-channel/', add_channel, name='add-channel'),
]