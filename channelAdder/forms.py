from django import forms

class Record_Form(forms.Form):
    message = forms.CharField(widget=forms.TextInput(attrs={'type': 'text',
        'class' : 'form-control'}), label="Notification Channel Name")