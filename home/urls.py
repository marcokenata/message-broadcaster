from django.conf.urls import url
from .views import *
#url for app

app_name = 'home'

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add-message/', add_message, name='add-message'),
    url(r'^add-message-api/', add_message_api, name="add_message_api"),
]