from django.shortcuts import render
from .tasks import publisher
from .forms import Record_Form
import datetime
from .models import Messages
from channelAdder.models import Routers
from django.http import HttpResponseRedirect
from django.utils import timezone
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

# Create your views here.
response = {}


def index(request):
    response['forms'] = Record_Form
    return render(request, 'index.html', response)


def add_message(request):
    form = Record_Form(request.POST or None)
    if (request.method == 'POST'):
        messageId = -999
        response['message'] = request.POST['message']
        response['routingKey'] = request.POST['notification_channel']
        response['routing_channel'] = request.POST['priority']
        response['datetime'] = datetime.datetime.strptime(
            request.POST['date'], '%Y-%m-%dT%H:%M')
        imageProcess = request.FILES['image'] if 'image' in request.FILES else False
        if (imageProcess == False):
            response['image'] = 'noImage'
        else:
            response['image'] = request.FILES.get('image')
        current_tz = timezone.get_current_timezone()
        t2 = current_tz.localize(response['datetime'])
        todo = Messages(routingKey=Routers.objects.get(
            routingKey=response['routingKey']), message=response['message'], dateTime=t2, image=response['image'], messageType=response['routing_channel'])
        todo.save()
        messageId = todo.image.url
        print(messageId)
        Routers.objects.filter(routingKey=response['routingKey']).update(
            count=Routers.objects.get(routingKey=response['routingKey']).count + 1)
        
        publisher.apply_async(args=[response['routingKey']+"."+response['routing_channel'],
                                    response['message'], messageId, response['routing_channel']], eta=t2)
        return HttpResponseRedirect('/home/')
    else:
        return HttpResponseRedirect('/home/')


@api_view(['POST'])
def add_message_api(request):
    if request.method == 'POST':
        messageId = -999
        response['message'] = request.POST['message']
        response['datetime'] = request.POST['dateTime']
        response['routingKey'] = request.POST['channel']
        response['messageType'] = request.POST['messageType']
        response['datetime'] = datetime.datetime.strptime(
            request.POST['dateTime'], '%Y-%m-%dT%H:%M')
        imageProcess = request.FILES['imageUri'] if 'imageUri' in request.FILES else False
        if (imageProcess == False):
            response['image'] = 'noImage'
        else:
            response['image'] = request.FILES['imageUri']
        current_tz = timezone.get_current_timezone()
        t2 = current_tz.localize(response['datetime'])
        todo = Messages(routingKey=Routers.objects.get(
            routingKey=response['routingKey']), message=response['message'], dateTime=t2, image=response['image'],messageType=response['messageType'])
        todo.save()
        messageId = todo.image.url
        Routers.objects.filter(routingKey=response['routingKey']).update(
            count=Routers.objects.get(routingKey=response['routingKey']).count + 1)
        publisher.apply_async(
            args=[response['routingKey']+"."+response['messageType'], response['message'], messageId, response['messageType']], eta=t2)
        return Response(status=status.HTTP_201_CREATED)
