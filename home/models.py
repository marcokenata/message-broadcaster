from django.db import models
from channelAdder.models import Routers
# Create your models here
class Messages(models.Model):
    routingKey = models.ForeignKey(Routers, default=None, on_delete=models.CASCADE,related_name="router")
    dateTime = models.DateTimeField()
    message = models.CharField(max_length=100000,blank=True)
    messageType = models.CharField(max_length=255, blank=True)
    image = models.FileField(upload_to='images/', default="noImage")

    @property	
    def photo_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url