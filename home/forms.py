from django import forms
from .models import Routers

class Record_Form(forms.Form):
    message = forms.CharField(widget=forms.Textarea(attrs={'type': 'text',
		'cols' : 90,
		'rows' : 10,
        'class' : 'form-control'}), label="Message")
    date = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local', 'class': 'form-control' }))
    notification_channel = forms.ModelChoiceField(queryset=Routers.objects.values_list("routingKey",flat=True).distinct(),empty_label=None,label="Notification Channel:",widget=forms.Select(attrs={'class':'form-control'}),required=True)
    priority = forms.ChoiceField(choices=[('Important','Important'),('General','General'),('Targeted','Targeted'),('All Devices','All Devices')],label="Message Type",widget=forms.Select(attrs={'class':'form-control'}),required=True)
    image = forms.ImageField(widget=forms.ClearableFileInput(attrs={'class':'form-control-file'}),label="Notification Image",required=False)