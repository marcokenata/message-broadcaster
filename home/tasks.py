from celery import shared_task
from kombu import Connection, Exchange, Producer, Queue
from messageBroadcaster.settings import CELERY_BROKER_URL

import pika
import sys
import json

@shared_task
def publisher(routing_key1, message, imageUrl, priority):
    conn = Connection(CELERY_BROKER_URL)
    channel1 = conn.channel()

    exchange1 = Exchange(name='notifications12', type='topic', durable=True)
    producer = Producer(exchange=exchange1, channel=channel1,
                        routing_key=routing_key1)

    exchangeAll = Exchange(name='allmessage', type='fanout', durable=True)
    producerAll = Producer(exchange=exchangeAll, channel=channel1,
                        routing_key=routing_key1)
    # queue = Queue(name='queue1', exchange=exchange1,
    #               routing_key=routing_key1, exclusive=False)
    # queue.maybe_bind(conn)
    # queue.declare()

    messageFinal = {}
    messageFinal['imageUrl'] = imageUrl
    messageFinal['message'] = message
    messageFinal['priority'] = priority

    print(json.dumps(messageFinal))
    print(message)

    routingSplit = routing_key1.split(".")
    if(routingSplit[1] == "All Devices"):
        producerAll.publish(json.dumps(messageFinal))
    else:
        producer.publish(json.dumps(messageFinal))

    # connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    # channel = connection.channel()

    # channel.exchange_declare(exchange='notifications',exchange_type='topic')

    # channel.basic_publish(exchange='notifications',routing_key=routing_key,body=message)
    # connection.close()
