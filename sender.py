import pika
import sys

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.exchange_declare(exchange='notifications12', exchange_type='topic',durable=True)
channel.exchange_declare(exchange='allmessage', exchange_type='fanout',durable=True)


routing_key = 'test.b.a'
message = 'Hello World!'
if (sys.argv[1] == "2"):
    channel.basic_publish(exchange='notifications12', routing_key=routing_key, body=message)
elif (sys.argv[1] == "1"):
    channel.basic_publish(exchange='allmessage', routing_key=routing_key, body=message)
print(" [x] Sent %r:%r" % (routing_key, message))
connection.close()