import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'messageBroadcaster.settings')

app = Celery('messageBroadcaster')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()